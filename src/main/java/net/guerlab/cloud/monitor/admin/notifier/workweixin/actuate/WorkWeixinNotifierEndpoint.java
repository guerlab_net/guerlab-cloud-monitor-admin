/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.workweixin.actuate;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;

import net.guerlab.cloud.monitor.admin.notifier.AbstractNotifierEndpoint;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.WorkWeixinNotifier;

/**
 * 企业微信通知监控端点.
 *
 * @author guer
 */
@SuppressWarnings("unused")
@Endpoint(id = "work-weixin-notifier")
public class WorkWeixinNotifierEndpoint extends AbstractNotifierEndpoint<WorkWeixinNotifier> {

	/**
	 * 创建实例.
	 *
	 * @param notifier 企业微信通知实现
	 */
	public WorkWeixinNotifierEndpoint(WorkWeixinNotifier notifier) {
		super(notifier);
	}
}
