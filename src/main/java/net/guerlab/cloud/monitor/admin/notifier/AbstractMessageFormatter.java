/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import lombok.extern.slf4j.Slf4j;

import org.springframework.lang.Nullable;

/**
 * 抽象消息格式化程序.
 *
 * @param <P> 通知配置类型
 * @author guer
 */
@Slf4j
public abstract class AbstractMessageFormatter<P extends AbstractNotifierProperties>
		implements MessageFormatter<P> {

	protected final static String INSTANCE_URL_FORMAT = "[%s](%s)";

	/**
	 * 标题格式化内容.
	 */
	protected final String titleFormat;

	/**
	 * 内容格式化内容.
	 */
	protected final String contentFormat;

	/**
	 * 语言包.
	 */
	protected final Properties langProperties;

	/**
	 * 创建实例.
	 */
	public AbstractMessageFormatter() {
		ClassLoader loader = AbstractMessageFormatter.class.getClassLoader();
		this.titleFormat = readFile(loader, "title", ".txt");
		this.contentFormat = readFile(loader, "content", ".md");
		this.langProperties = loadLangProperties(loader);
	}

	/**
	 * 语言包格式化.
	 *
	 * @param key          关键字
	 * @param properties   配置列表
	 * @param defaultValue 默认值
	 * @return 格式化后的内容
	 */
	protected static String langFormat(String key, @Nullable Properties properties, @Nullable String defaultValue) {
		if (properties == null) {
			return key;
		}

		String value = properties.getProperty(key);
		return value != null ? value : defaultValue != null ? defaultValue : key;
	}

	@Nullable
	private static String tryReadFile(ClassLoader loader, String fileName, String suffix) {
		InputStream inputStream = loader.getResourceAsStream(fileName + suffix);
		if (inputStream == null) {
			return null;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		return reader.lines().collect(Collectors.joining("\n"));
	}

	private static String format0(String format, Map<String, String> params) {
		String content = format;

		for (Map.Entry<String, String> entry : params.entrySet()) {
			String key = "\\$\\{" + entry.getKey() + "}";
			String value = entry.getValue();
			content = content.replaceAll(key, value);
		}

		return content;
	}

	@Nullable
	private Properties loadLangProperties(ClassLoader loader) {
		String statusProperties = tryReadFiles(loader, "lang", ".properties");
		if (statusProperties == null) {
			return null;
		}

		Properties properties = new Properties();
		try {
			properties.load(new ByteArrayInputStream(statusProperties.getBytes()));
		}
		catch (Exception e) {
			log.debug(e.getLocalizedMessage(), e);
			return null;
		}
		return properties;
	}

	private String readFile(ClassLoader loader, String fileName, String suffix) {
		String content = tryReadFiles(loader, fileName, suffix);

		if (content == null) {
			throw new RuntimeException("read " + fileName + "." + suffix + "fail");
		}

		return content;
	}

	@Nullable
	private String tryReadFiles(ClassLoader loader, String fileName, String suffix) {
		Locale locale = Locale.getDefault();

		String[] paths = new String[] {
				getBasePath() + fileName + "." + locale.getLanguage(),
				getBasePath() + fileName,
				"classpath:/" + getBasePath() + fileName + "." + locale.getLanguage(),
				"classpath:/" + getBasePath() + fileName};

		String content;
		for (String path : paths) {
			content = tryReadFile(loader, path, suffix);
			if (content != null) {
				return content;
			}
		}
		return null;
	}

	/**
	 * 获取基础路径.
	 *
	 * @return 基础路径
	 */
	protected abstract String getBasePath();

	@Override
	public final FormatResult format(InstanceStatusChangedEvent event, Instance instance, P properties) {
		Map<String, String> params = buildParams(event, instance, properties);
		FormatResult result = new FormatResult();
		result.setTitle(format0(titleFormat, params));
		result.setContent(format0(contentFormat, params).replace("<br>", "\n"));
		return result;
	}

	/**
	 * 构造参数表.
	 *
	 * @param event      事件
	 * @param instance   实例
	 * @param properties 配置文件
	 * @return 参数表
	 */
	protected abstract Map<String, String> buildParams(InstanceStatusChangedEvent event, Instance instance,
			P properties);
}
