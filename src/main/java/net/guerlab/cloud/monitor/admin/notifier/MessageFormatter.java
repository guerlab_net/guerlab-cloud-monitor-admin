/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;

/**
 * 消息格式化程序.
 *
 * @param <PROPERTIES> 格式化配置类型
 * @author guer
 */
public interface MessageFormatter<PROPERTIES> {

	/**
	 * 格式化内容.
	 *
	 * @param event      事件
	 * @param instance   实例
	 * @param properties 配置文件
	 * @return 内容
	 */
	FormatResult format(InstanceStatusChangedEvent event, Instance instance, PROPERTIES properties);

}
