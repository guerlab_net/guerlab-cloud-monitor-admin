/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.dingtalk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 被@的人的信息.
 *
 * @author guer
 */
@Data
public class At {

	/**
	 * 是否@所有人.
	 */
	@JsonProperty("isAtAll")
	private boolean atAll;

	/**
	 * 被@人的手机号.
	 */
	private List<String> atMobiles;
}
