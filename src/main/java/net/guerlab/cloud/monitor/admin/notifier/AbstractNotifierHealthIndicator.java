/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;

/**
 * 抽象通知健康状态.
 *
 * @param <P> 通知配置类型
 * @author guer
 */
public abstract class AbstractNotifierHealthIndicator<P extends AbstractNotifierProperties> extends AbstractHealthIndicator {

	private final P properties;

	/**
	 * 创建实例.
	 *
	 * @param properties 通知配置.
	 */
	public AbstractNotifierHealthIndicator(P properties) {
		this.properties = properties;
	}

	@Override
	protected void doHealthCheck(Health.Builder builder) {
		builder.up().withDetail("notifyUrl", properties.getNotifyUrl());
	}
}
