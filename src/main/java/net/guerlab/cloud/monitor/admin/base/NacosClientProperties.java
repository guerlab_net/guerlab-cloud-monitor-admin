/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.base;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 认证配置.
 *
 * @author guer
 */
@Data
@Component
@ConfigurationProperties(prefix = "nacos")
public class NacosClientProperties {

	/**
	 * NACOS主机名称.
	 */
	private String hostname = "nacos";

	/**
	 * NACOS端口.
	 */
	private Integer port = 8848;

	/**
	 * 命名空间.
	 */
	private String namespace = "";
}
