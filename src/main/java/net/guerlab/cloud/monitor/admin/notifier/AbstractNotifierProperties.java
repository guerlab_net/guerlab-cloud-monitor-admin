/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import lombok.Data;

/**
 * 抽象通知配置.
 *
 * @author guer
 */
@Data
public abstract class AbstractNotifierProperties {

	/**
	 * 通知链接.
	 */
	private String notifyUrl;

	/**
	 * Spring Boot Admin访问基础路径.
	 */
	private String baseUrl;
}
