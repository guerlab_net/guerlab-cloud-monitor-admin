/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.base;

import java.util.UUID;

import de.codecentric.boot.admin.server.config.AdminServerProperties;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * 安全配置.
 *
 * @author guer
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(NacosClientProperties.class)
public class SecuritySecureConfig extends WebSecurityConfigurerAdapter {

	private final AdminServerProperties adminServer;

	/**
	 * 创建实例.
	 *
	 * @param adminServer AdminServerProperties
	 */
	public SecuritySecureConfig(AdminServerProperties adminServer) {
		this.adminServer = adminServer;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		SavedRequestAwareAuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();
		successHandler.setTargetUrlParameter("redirectTo");
		successHandler.setDefaultTargetUrl(this.adminServer.path("/"));

		http.csrf().ignoringAntMatchers("/actuator/**");

		http.authorizeRequests((authorizeRequests) ->
						authorizeRequests
								.antMatchers(this.adminServer.path("/assets/**")).permitAll()
								.antMatchers(this.adminServer.path("/actuator/**")).permitAll()
								.antMatchers(this.adminServer.path("/login")).permitAll()
								.antMatchers(this.adminServer.path("/instances/*/actuator/metrics/**")).permitAll()
								.anyRequest().authenticated()
				)
				.formLogin((formLogin) -> formLogin.loginPage(this.adminServer.path("/login"))
						.successHandler(successHandler).and())
				.logout((logout) -> logout.logoutUrl(this.adminServer.path("/logout")))
				.httpBasic(Customizer.withDefaults())
				.csrf((csrf) ->
						csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
								.ignoringRequestMatchers(
										new AntPathRequestMatcher(this.adminServer.path("/instances"), HttpMethod.POST.toString()),
										new AntPathRequestMatcher(this.adminServer.path("/instances/*"), HttpMethod.DELETE.toString())
								)
				)
				.rememberMe((rememberMe) -> rememberMe.key(UUID.randomUUID().toString()).tokenValiditySeconds(1209600));
	}

}
