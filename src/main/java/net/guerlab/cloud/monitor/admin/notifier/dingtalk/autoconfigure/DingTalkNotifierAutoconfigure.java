/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.dingtalk.autoconfigure;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.codecentric.boot.admin.server.config.AdminServerNotifierAutoConfiguration;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.guerlab.cloud.monitor.admin.notifier.dingtalk.DingTalkNotifier;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.DingTalkNotifierProperties;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.actuate.DingTalkHealthIndicator;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.actuate.DingTalkNotifierEndpoint;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.formatter.DefaultDingTalkMessageFormatter;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.formatter.DingTalkMessageFormatter;

/**
 * 钉钉通知自动配置.
 *
 * @author guer
 */
@Configuration
@EnableConfigurationProperties(DingTalkNotifierProperties.class)
@ConditionalOnProperty(prefix = DingTalkNotifierProperties.PREFIX, name = "enabled", havingValue = "true")
@AutoConfigureBefore({AdminServerNotifierAutoConfiguration.NotifierTriggerConfiguration.class,
		AdminServerNotifierAutoConfiguration.CompositeNotifierConfiguration.class})
public class DingTalkNotifierAutoconfigure {

	/**
	 * 构造钉钉消息通知.
	 *
	 * @param repository   实例Repository
	 * @param properties   钉钉消息通知配置
	 * @param formatter    钉钉消息格式化程序
	 * @param objectMapper ObjectMapper
	 * @return 钉钉消息通知
	 */
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	@Bean
	public DingTalkNotifier dingTalkNotifier(InstanceRepository repository, DingTalkNotifierProperties properties,
			DingTalkMessageFormatter formatter, ObjectMapper objectMapper) {
		return new DingTalkNotifier(repository, properties, formatter, objectMapper);
	}

	/**
	 * 构造钉钉通知健康状态.
	 *
	 * @param properties 钉钉消息通知配置
	 * @return 钉钉通知健康状态
	 */
	@Bean
	public DingTalkHealthIndicator dingTalkHealthIndicator(DingTalkNotifierProperties properties) {
		return new DingTalkHealthIndicator(properties);
	}

	/**
	 * 钉钉通知监控端点.
	 *
	 * @param dingTalkNotifier 钉钉通知实现
	 * @return 钉钉通知监控端点
	 */
	@Bean
	public DingTalkNotifierEndpoint dingTalkNotifierEndpoint(DingTalkNotifier dingTalkNotifier) {
		return new DingTalkNotifierEndpoint(dingTalkNotifier);
	}

	/**
	 * 构造钉钉消息格式化程序.
	 *
	 * @return 钉钉消息格式化程序
	 */
	@Bean
	@ConditionalOnMissingBean
	public DingTalkMessageFormatter dingTalkMessageFormatter() {
		return new DefaultDingTalkMessageFormatter();
	}
}
