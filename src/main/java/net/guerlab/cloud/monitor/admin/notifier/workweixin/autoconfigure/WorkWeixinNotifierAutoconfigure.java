/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.workweixin.autoconfigure;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.codecentric.boot.admin.server.config.AdminServerNotifierAutoConfiguration;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.guerlab.cloud.monitor.admin.notifier.workweixin.WorkWeixinNotifier;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.WorkWeixinNotifierProperties;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.actuate.WorkWeixinHealthIndicator;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.actuate.WorkWeixinNotifierEndpoint;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.formatter.DefaultWorkWeixinMessageFormatter;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.formatter.WorkWeixinMessageFormatter;

/**
 * 企业微信通知自动配置.
 *
 * @author guer
 */
@Configuration
@EnableConfigurationProperties(WorkWeixinNotifierProperties.class)
@ConditionalOnProperty(prefix = WorkWeixinNotifierProperties.PREFIX, name = "enabled", havingValue = "true")
@AutoConfigureBefore({AdminServerNotifierAutoConfiguration.NotifierTriggerConfiguration.class,
		AdminServerNotifierAutoConfiguration.CompositeNotifierConfiguration.class})
public class WorkWeixinNotifierAutoconfigure {

	/**
	 * 构造企业微信消息通知.
	 *
	 * @param repository   实例Repository
	 * @param properties   企业微信消息通知配置
	 * @param formatter    企业微信消息格式化程序
	 * @param objectMapper ObjectMapper
	 * @return 企业微信消息通知
	 */
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	@Bean
	public WorkWeixinNotifier workWeixinNotifier(InstanceRepository repository, WorkWeixinNotifierProperties properties,
			WorkWeixinMessageFormatter formatter, ObjectMapper objectMapper) {
		return new WorkWeixinNotifier(repository, properties, formatter, objectMapper);
	}

	/**
	 * 构造企业微信通知健康状态.
	 *
	 * @param properties 企业微信消息通知配置
	 * @return 企业微信通知健康状态
	 */
	@Bean
	public WorkWeixinHealthIndicator workWeixinHealthIndicator(WorkWeixinNotifierProperties properties) {
		return new WorkWeixinHealthIndicator(properties);
	}

	/**
	 * 企业微信通知监控端点.
	 *
	 * @param workWeixinNotifier 企业微信通知实现
	 * @return 企业微信通知监控端点
	 */
	@Bean
	public WorkWeixinNotifierEndpoint workWeixinNotifierEndpoint(WorkWeixinNotifier workWeixinNotifier) {
		return new WorkWeixinNotifierEndpoint(workWeixinNotifier);
	}

	/**
	 * 构造企业微信消息格式化程序.
	 *
	 * @return 企业微信消息格式化程序
	 */
	@Bean
	@ConditionalOnMissingBean
	public WorkWeixinMessageFormatter workWeixinMessageFormatter() {
		return new DefaultWorkWeixinMessageFormatter();
	}
}
