/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.dingtalk.actuate;

import net.guerlab.cloud.monitor.admin.notifier.AbstractNotifierHealthIndicator;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.DingTalkNotifierProperties;

/**
 * 钉钉通知健康状态.
 *
 * @author guer
 */
public class DingTalkHealthIndicator extends AbstractNotifierHealthIndicator<DingTalkNotifierProperties> {

	/**
	 * 创建实例.
	 *
	 * @param properties 钉钉通知配置
	 */
	public DingTalkHealthIndicator(DingTalkNotifierProperties properties) {
		super(properties);
	}
}
