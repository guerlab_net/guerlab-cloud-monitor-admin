/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.events.InstanceEvent;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.notify.AbstractStatusChangeNotifier;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import org.springframework.util.Assert;

/**
 * 企业微信通知实现.
 *
 * @param <P> 格式化配置类型
 * @param <F> 格式化程序
 * @author guer
 */
@Slf4j
public abstract class AbstractNotifier<P extends AbstractNotifierProperties, F extends MessageFormatter<P>>
		extends AbstractStatusChangeNotifier {

	/**
	 * HttpClient.
	 */
	protected final HttpClient httpClient = HttpClient.newHttpClient();

	/**
	 * 格式化配置.
	 */
	@Getter
	protected final P properties;

	/**
	 * 格式化程序.
	 */
	@Getter
	protected final F formatter;

	/**
	 * ObjectMapper.
	 */
	protected final ObjectMapper objectMapper;

	private final String notifierName;

	/**
	 * 创建实例.
	 *
	 * @param notifierName 通知器名称.
	 * @param repository   InstanceRepository
	 * @param properties   通知器配置
	 * @param formatter    格式化程序
	 * @param objectMapper ObjectMapper
	 */
	protected AbstractNotifier(String notifierName, InstanceRepository repository, P properties,
			F formatter, ObjectMapper objectMapper) {
		super(repository);
		this.notifierName = notifierName;
		Assert.hasText(properties.getNotifyUrl(), "notify url can not be empty");
		this.properties = properties;
		this.formatter = formatter;
		this.objectMapper = objectMapper;
	}

	@Override
	protected boolean shouldNotify(InstanceEvent event, Instance instance) {
		return super.shouldNotify(event, instance) && event instanceof InstanceStatusChangedEvent;
	}

	@Override
	protected Mono<Void> doNotify(InstanceEvent event, Instance instance) {
		return Mono.fromRunnable(() -> {
			try {
				notifyHandler((InstanceStatusChangedEvent) event, instance);
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new RuntimeException("Error sending " + getClass().getSimpleName() + " notification", e);
			}
		});
	}

	/**
	 * 推送消息.
	 *
	 * @param event    实例状态变更事件
	 * @param instance 实例
	 * @throws Exception 推送失败抛出异常
	 */
	public void notifyHandler(InstanceStatusChangedEvent event, Instance instance) throws Exception {
		String notifyUrl = buildNotifyUrl();

		FormatResult formatResult = formatter.format(event, instance, properties);

		String body = getRequestBody(formatResult);

		log.info("notify: {}, body: {}", notifierName, body);

		HttpRequest request = HttpRequest.newBuilder(URI.create(notifyUrl))
				.POST(HttpRequest.BodyPublishers.ofString(body)).header("Content-Type", "application/json").build();

		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		log.debug("response: {}", response.body());
	}

	/**
	 * 构造通知地址.
	 *
	 * @return 通知地址
	 * @throws Exception 当构造请求body失败的时候抛出异常
	 */
	protected abstract String buildNotifyUrl() throws Exception;

	/**
	 * 获取请求body.
	 *
	 * @param formatResult 格式化结果
	 * @return 请求body
	 * @throws Exception 当构造请求body失败的时候抛出异常
	 */
	protected abstract String getRequestBody(FormatResult formatResult) throws Exception;
}
