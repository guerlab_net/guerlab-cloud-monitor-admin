/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.dingtalk;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import lombok.extern.slf4j.Slf4j;

import org.springframework.util.Assert;

import net.guerlab.cloud.monitor.admin.notifier.AbstractNotifier;
import net.guerlab.cloud.monitor.admin.notifier.FormatResult;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.domain.At;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.domain.Markdown;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.domain.Message;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.formatter.DingTalkMessageFormatter;

/**
 * 钉钉通知实现.
 *
 * @author guer
 */
@Slf4j
public class DingTalkNotifier extends AbstractNotifier<DingTalkNotifierProperties, DingTalkMessageFormatter> {

	/**
	 * 创建实例.
	 *
	 * @param repository   InstanceRepository
	 * @param properties   钉钉通知配置
	 * @param formatter    钉钉消息格式化程序
	 * @param objectMapper ObjectMapper
	 */
	public DingTalkNotifier(InstanceRepository repository, DingTalkNotifierProperties properties,
			DingTalkMessageFormatter formatter, ObjectMapper objectMapper) {
		super("DingTalk", repository, properties, formatter, objectMapper);
		Assert.hasText(properties.getSecret(), "secret can not be empty");
		log.info("enable dingTalk notifier");
	}

	private static String buildSign(Long timestamp, String secret)
			throws NoSuchAlgorithmException, InvalidKeyException {
		String stringToSign = timestamp + "\n" + secret;
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256"));
		byte[] signData = mac.doFinal(stringToSign.getBytes(StandardCharsets.UTF_8));
		return URLEncoder.encode(new String(Base64.getEncoder().encode(signData)), StandardCharsets.UTF_8);
	}

	@Override
	protected String buildNotifyUrl() throws Exception {
		Long timestamp = System.currentTimeMillis();
		String sign = buildSign(timestamp, properties.getSecret());
		return properties.getNotifyUrl() + "&timestamp=" + timestamp + "&sign=" + sign;
	}

	@Override
	protected String getRequestBody(FormatResult formatResult) throws Exception {
		Markdown markDown = new Markdown();
		markDown.setTitle(formatResult.getTitle());
		markDown.setText(formatResult.getContent());

		At at = new At();
		at.setAtAll(properties.isAtAll());
		at.setAtMobiles(properties.getAtMobiles());

		Message message = new Message();
		message.setMarkdown(markDown);
		message.setAt(at);

		return objectMapper.writeValueAsString(message);
	}
}
