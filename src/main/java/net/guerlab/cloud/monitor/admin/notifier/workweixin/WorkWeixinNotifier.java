/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.workweixin;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import lombok.extern.slf4j.Slf4j;

import net.guerlab.cloud.monitor.admin.notifier.AbstractNotifier;
import net.guerlab.cloud.monitor.admin.notifier.FormatResult;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.domain.Markdown;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.domain.Message;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.formatter.WorkWeixinMessageFormatter;

/**
 * 企业微信通知实现.
 *
 * @author guer
 */
@Slf4j
public class WorkWeixinNotifier extends AbstractNotifier<WorkWeixinNotifierProperties, WorkWeixinMessageFormatter> {

	/**
	 * 创建实例.
	 *
	 * @param repository   InstanceRepository
	 * @param properties   企业微信通知配置
	 * @param formatter    企业微信消息格式化程序
	 * @param objectMapper ObjectMapper
	 */
	public WorkWeixinNotifier(InstanceRepository repository, WorkWeixinNotifierProperties properties,
			WorkWeixinMessageFormatter formatter, ObjectMapper objectMapper) {
		super("WorkerWeixin", repository, properties, formatter, objectMapper);
		log.info("enable WorkWeixin notifier");
	}

	@Override
	protected String buildNotifyUrl() {
		return properties.getNotifyUrl();
	}

	@Override
	protected String getRequestBody(FormatResult formatResult) throws Exception {
		Markdown markDown = new Markdown();
		markDown.setContent(formatResult.getContent());

		Message message = new Message();
		message.setMarkdown(markDown);

		return objectMapper.writeValueAsString(message);
	}
}
