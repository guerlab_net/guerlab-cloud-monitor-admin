/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.dingtalk.formatter;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import lombok.extern.slf4j.Slf4j;

import org.springframework.util.CollectionUtils;

import net.guerlab.cloud.monitor.admin.notifier.AbstractMessageFormatter;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.DingTalkNotifierProperties;

/**
 * 默认钉钉消息格式化程序.
 *
 * @author guer
 */
@Slf4j
public class DefaultDingTalkMessageFormatter extends AbstractMessageFormatter<DingTalkNotifierProperties>
		implements DingTalkMessageFormatter {

	private final static String BASE_PATH = "META-INF/spring-boot-admin-server/dingTalk/";

	@Override
	protected String getBasePath() {
		return BASE_PATH;
	}

	@Override
	protected Map<String, String> buildParams(InstanceStatusChangedEvent event, Instance instance,
			DingTalkNotifierProperties properties) {
		String serviceName = instance.getRegistration().getName();
		String instanceId = instance.getId().getValue();
		String formStatus = instance.getStatusInfo().getStatus();
		String toStatus = event.getStatusInfo().getStatus();

		String instanceUrl;
		if (properties.getBaseUrl() == null) {
			instanceUrl = instanceId;
		}
		else {
			instanceUrl = String.format(INSTANCE_URL_FORMAT, instanceId,
					properties.getBaseUrl() + "/instances/" + instanceId + "/details");
		}
		String at;
		if (properties.isAtAll()) {
			at = langFormat("atAll", langProperties, "@all");
		}
		else if (!CollectionUtils.isEmpty(properties.getAtMobiles())) {
			at = properties.getAtMobiles().stream().map(mobile -> "@" + mobile).collect(Collectors.joining(","));
		}
		else {
			at = "";
		}

		Map<String, String> params = new HashMap<>(8);
		params.put("serviceName", serviceName);
		params.put("instanceId", instanceId);
		params.put("formStatus", langFormat(formStatus, langProperties, null));
		params.put("toStatus", langFormat(toStatus, langProperties, null));
		params.put("instanceUrl", instanceUrl);
		params.put("at", at);

		return params;
	}
}
