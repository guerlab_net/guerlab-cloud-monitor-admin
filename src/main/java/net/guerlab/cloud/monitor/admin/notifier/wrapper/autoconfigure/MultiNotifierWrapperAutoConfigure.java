/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.wrapper.autoconfigure;

import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.notify.Notifier;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import net.guerlab.cloud.monitor.admin.notifier.wrapper.MultiNotifierWrapper;

/**
 * 多通知者对象包装器自动配置.
 *
 * @author guer
 */
@Configuration
public class MultiNotifierWrapperAutoConfigure {

	/**
	 * 构造多通知者对象包装器.
	 *
	 * @param repository 实例Repository
	 * @param provider   通知者包装列表
	 * @return 多通知者对象包装器
	 */
	@Bean
	@Primary
	public MultiNotifierWrapper multiNotifierWrapper(InstanceRepository repository, ObjectProvider<Notifier> provider) {
		return new MultiNotifierWrapper(repository, provider);
	}
}
