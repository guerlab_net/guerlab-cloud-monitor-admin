/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.wrapper;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.events.InstanceEvent;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.domain.values.StatusInfo;
import de.codecentric.boot.admin.server.notify.AbstractStatusChangeNotifier;
import de.codecentric.boot.admin.server.notify.Notifier;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.beans.factory.ObjectProvider;

/**
 * 多通知者对象包装器.
 *
 * @author guer
 */
@Slf4j
public class MultiNotifierWrapper extends AbstractStatusChangeNotifier {

	private final ObjectProvider<Notifier> provider;

	/**
	 * 创建实例.
	 *
	 * @param repository InstanceRepository
	 * @param provider   Notifier's ObjectProvider
	 */
	public MultiNotifierWrapper(InstanceRepository repository, ObjectProvider<Notifier> provider) {
		super(repository);
		this.provider = provider;
		provider.stream().forEach(notifier -> log.debug("wrapper notifier: {}", notifier));
	}

	@Override
	protected Mono<Void> doNotify(InstanceEvent event, Instance instance) {
		if (event instanceof InstanceStatusChangedEvent statusChangedEvent) {
			StatusInfo statusInfo = statusChangedEvent.getStatusInfo();
			log.info("invoke event[name: {}, instanceId: {}, status: {}]", instance.getRegistration().getName(),
					instance.getId(), statusInfo.getStatus());

			return Flux.fromIterable(provider).filter(notifier -> !(notifier instanceof MultiNotifierWrapper))
					.flatMap(notifier -> notifier.notify(event)).collectList().then();
		}
		else {
			log.info("is not InstanceStatusChangedEvent");
			return Mono.empty();
		}
	}
}
