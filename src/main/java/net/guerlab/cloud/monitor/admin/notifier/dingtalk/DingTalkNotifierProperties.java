/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.dingtalk;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.boot.context.properties.ConfigurationProperties;

import net.guerlab.cloud.monitor.admin.notifier.AbstractNotifierProperties;

/**
 * 钉钉通知配置.
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = DingTalkNotifierProperties.PREFIX)
public class DingTalkNotifierProperties extends AbstractNotifierProperties {

	/**
	 * 配置前缀.
	 */
	public static final String PREFIX = "spring.boot.admin.notify.ding-talk";

	/**
	 * 密钥.
	 */
	private String secret;

	/**
	 * 是否@所有人.
	 */
	private boolean atAll;

	/**
	 * 被@人的手机号列表.
	 */
	private List<String> atMobiles;
}
