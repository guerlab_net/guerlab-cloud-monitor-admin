/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import java.util.HashMap;
import java.util.Map;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.domain.values.InstanceId;
import de.codecentric.boot.admin.server.domain.values.Registration;
import de.codecentric.boot.admin.server.domain.values.StatusInfo;

import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;

/**
 * 抽象通知监控端点.
 *
 * @param <N> 通知器类型
 * @author guer
 */
@SuppressWarnings("unused")
public abstract class AbstractNotifierEndpoint<N extends AbstractNotifier<?, ?>> {

	private final N notifier;

	/**
	 * 创建实例.
	 *
	 * @param notifier 通知器实例
	 */
	public AbstractNotifierEndpoint(N notifier) {
		this.notifier = notifier;
	}

	@ReadOperation
	public Map<String, Object> get() {
		Map<String, Object> result = new HashMap<>(2);
		result.put("formatter", notifier.getFormatter().getClass().getName());
		result.put("properties", notifier.getProperties());

		return result;
	}

	@WriteOperation
	public void push() throws Exception {
		InstanceId instanceId = InstanceId.of("test");
		InstanceStatusChangedEvent event = new InstanceStatusChangedEvent(instanceId, -1L, StatusInfo.ofDown());

		Registration registration = Registration.create("test", "https://test.com/health").build();

		Instance instance = Instance.create(instanceId).withStatusInfo(StatusInfo.ofUp()).register(registration);

		notifier.notifyHandler(event, instance);
	}
}
