/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import java.util.function.BiFunction;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.values.InstanceId;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author guer
 */
@AllArgsConstructor
public class TestInstanceRepository implements InstanceRepository {

	private final Instance instance;

	@Override
	public Mono<Instance> save(Instance app) {
		return Mono.just(instance);
	}

	@Override
	public Flux<Instance> findAll() {
		return Flux.just(instance);
	}

	@Override
	public Mono<Instance> find(InstanceId id) {
		return Mono.just(instance);
	}

	@Override
	public Flux<Instance> findByName(String name) {
		return Flux.just(instance);
	}

	@Override
	public Mono<Instance> compute(InstanceId id, BiFunction<InstanceId, Instance, Mono<Instance>> remappingFunction) {
		return Mono.just(instance);
	}

	@Override
	public Mono<Instance> computeIfPresent(InstanceId id, BiFunction<InstanceId, Instance, Mono<Instance>> remappingFunction) {
		return Mono.just(instance);
	}
}
