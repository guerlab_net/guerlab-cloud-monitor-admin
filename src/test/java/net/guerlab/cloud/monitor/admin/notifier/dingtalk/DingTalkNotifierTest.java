/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.dingtalk;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import net.guerlab.cloud.monitor.admin.notifier.AbstractNotifierTest;
import net.guerlab.cloud.monitor.admin.notifier.dingtalk.autoconfigure.DingTalkNotifierAutoconfigure;

/**
 * 钉钉通知测试
 *
 * @author guer
 */
@Slf4j
public class DingTalkNotifierTest extends AbstractNotifierTest<DingTalkNotifier> {

	@Test
	public void send() {
		super.send();
	}

	@Override
	protected String enabledProperties() {
		return "spring.boot.admin.notify.ding-talk.enabled=true";
	}

	@Override
	protected String propertiesFileName() {
		return "/dingTalkNotifierTest.properties.tmp";
	}

	@Override
	protected Class<?> autoconfigureClass() {
		return DingTalkNotifierAutoconfigure.class;
	}

	@Override
	protected Class<DingTalkNotifier> notifierClass() {
		return DingTalkNotifier.class;
	}
}
