/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.workweixin.formatter;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.domain.values.InstanceId;
import de.codecentric.boot.admin.server.domain.values.Registration;
import de.codecentric.boot.admin.server.domain.values.StatusInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import net.guerlab.cloud.monitor.admin.notifier.FormatResult;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.WorkWeixinNotifierProperties;

/**
 * @author guer
 */
class DefaultWorkWeixinMessageFormatterTests {

	@Test
	void buildParamsTest() {
		InstanceId instanceId = InstanceId.of("instanceId");

		InstanceStatusChangedEvent event = new InstanceStatusChangedEvent(instanceId, 0, StatusInfo.ofDown());

		Instance instance = Instance.create(instanceId)
				.register(Registration.create("Registration-name", "http://instance/healthUrl").build());

		WorkWeixinMessageFormatter formatter = new DefaultWorkWeixinMessageFormatter();

		WorkWeixinNotifierProperties properties = new WorkWeixinNotifierProperties();
		properties.setBaseUrl("http://admin-cp/");

		FormatResult formatResult = formatter.format(event, instance, properties);

		String titleTarget = "服务实例 Registration-name (instanceId) 状态变更为 [掉线(DOWN)]";
		String contentTarget = """
				### 服务状态变更

				> 1. 服务名：**Registration-name**
				> 2. 实例Id：**[instanceId](http://admin-cp//instances/instanceId/details)**
				> 3. 老状态：**未知(UNKNOWN)**
				> 4. 新状态：**掉线(DOWN)**""";

		Assertions.assertEquals(titleTarget, formatResult.getTitle());
		Assertions.assertEquals(contentTarget, formatResult.getContent());
	}
}
