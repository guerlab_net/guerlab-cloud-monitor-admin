/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.events.InstanceEvent;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.domain.values.InstanceId;
import de.codecentric.boot.admin.server.domain.values.Registration;
import de.codecentric.boot.admin.server.domain.values.StatusInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 抽象通知测试
 *
 * @param <N> 通知器类型
 * @author guer
 */
@Slf4j
public abstract class AbstractNotifierTest<N extends AbstractNotifier<?, ?>> {

	protected static AnnotationConfigApplicationContext context;

	protected static InstanceId testInstanceId;

	protected static Instance testInstance;

	protected static boolean testEnabled;

	@BeforeEach
	public void init() {
		context = new AnnotationConfigApplicationContext();

		BufferedReader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(AbstractNotifierTest.class.getResourceAsStream(propertiesFileName()))));

		reader.lines().forEach(line -> {
			log.debug("set properties: {}", line);
			TestPropertyValues.of(line).applyTo(context);

			if (line.contains(enabledProperties())) {
				testEnabled = true;
			}
		});

		testInstanceId = InstanceId.of("instanceId");

		Registration registration = Registration.builder().name("test").healthUrl("https://www.baidu.com/health")
				.build();
		testInstance = Instance.create(testInstanceId).register(registration);

		context.register(autoconfigureClass(), TestAutoConfigure.class, ObjectMapper.class);
		context.refresh();
	}

	public void send() {
		if (!testEnabled) {
			return;
		}
		N notifier = context.getBean(notifierClass());
		Assertions.assertNotNull(notifier);
		notifier.setIgnoreChanges(new String[] {});

		InstanceEvent instanceEvent = new InstanceStatusChangedEvent(testInstanceId, 1L, StatusInfo.ofUp());

		notifier.notify(instanceEvent).block();
	}

	protected abstract String enabledProperties();

	protected abstract String propertiesFileName();

	protected abstract Class<?> autoconfigureClass();

	protected abstract Class<N> notifierClass();

	@Configuration
	public static class TestAutoConfigure {

		@Bean
		public InstanceRepository testInstanceRepository() {
			return new TestInstanceRepository(testInstance);
		}
	}
}
