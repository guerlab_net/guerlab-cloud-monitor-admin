/*
 * Copyright 2018-2022 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.monitor.admin.notifier.workweixin;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import net.guerlab.cloud.monitor.admin.notifier.AbstractNotifierTest;
import net.guerlab.cloud.monitor.admin.notifier.workweixin.autoconfigure.WorkWeixinNotifierAutoconfigure;

/**
 * 企业微信通知测试
 *
 * @author guer
 */
@Slf4j
public class WorkWeixinNotifierTest extends AbstractNotifierTest<WorkWeixinNotifier> {

	@Test
	public void send() {
		super.send();
	}

	@Override
	protected String enabledProperties() {
		return "spring.boot.admin.notify.work-weixin.enabled=true";
	}

	@Override
	protected String propertiesFileName() {
		return "/workWeixinNotifierTest.properties.tmp";
	}

	@Override
	protected Class<?> autoconfigureClass() {
		return WorkWeixinNotifierAutoconfigure.class;
	}

	@Override
	protected Class<WorkWeixinNotifier> notifierClass() {
		return WorkWeixinNotifier.class;
	}
}
