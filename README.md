# Guerlab Cloud Monitor Admin

![](https://img.shields.io/badge/LICENSE-LGPL--3.0-brightgreen.svg)
![](https://img.shields.io/maven-central/v/net.guerlab.cloud.monitor.admin/guerlab-cloud-monitor-admin.svg)

基于spring boot admin的监控端，默认提供了邮件报警的接入和钉钉机器人webhook接入

## 注册中心与配置中心

- 注册中心: nacos
- 配置中心: nacos

## 环境变量

|名称|含义|默认值|
|:----|:----|:----|
|NACOS_HOSTNAME|nacos的服务器地址|nacos|
|NACOS_PORT|nacos的服务器端口|8848|
|NACOS_NAMESPACE|nacos所使用的命名空间|

## 配置文件

### 邮件配置

增加应用配置 admin.yml 用于支持当服务状态变更的时候进行邮件通知

```yaml
spring:
  mail:
    host: smtp.youmailldomain
    port: 465
    username: user@youmailldomain
    password: password
    properties:
      mail.smtp.auth: true
      mail.smtp.starttls.enable: true
      mail.smtp.starttls.required: true
      mail.smtp.socketFactory.port: 465
      mail.smtp.socketFactory.class: javax.net.ssl.SSLSocketFactory
      mail.smtp.socketFactory.fallback: false
  boot:
    admin:
      notify:
        mail:
          enabled: true
          from: Spring Boot Admin <user@youmailldomain>
          to:
            - you mail address
```

### 钉钉推送配置

增加应用配置 admin.yml 用于支持当服务状态变更的时候进行钉钉通知

```yaml
spring:
  boot:
    admin:
      notify:
        ding-talk:
          enabled: true #是否启用
          notify-url: #钉钉机器人webhook地址
          secret: #钉钉机器人访问密钥
          base-url: #Spring Boot Admin访问的基础路径
```

### 企业微信推送配置

增加应用配置 admin.yml 用于支持当服务状态变更的时候进企业微信通知

```yaml
spring:
  boot:
    admin:
      notify:
        work-weixin:
          enabled: true #是否启用
          notify-url: #企业微信机器人webhook地址
          base-url: #Spring Boot Admin访问的基础路径
```

### 安全配置

增加安全配置 security.yml 用于支持访问之前需要进行身份验证

```yaml
spring:
  security:
    user:
      name: user
      password: password
```
